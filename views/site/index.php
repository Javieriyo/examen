<?php

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $consulta1 array */ 

$this->title = 'My Yii Application';
?>

<div class="consulta1">
    <h1>Resultados de la consulta</h1>
    <?= GridView::widget([
        'dataProvider' => new \yii\data\ArrayDataProvider([
            'allModels' => $consulta1,
        ]),
        'columns' => [
            'nombre',
            'dorsal',
        ],
    ]); ?>
</div>

<div class="consulta2">
    <h1>Resultados de la consulta</h1>
    <?= GridView::widget([
        'dataProvider' => new \yii\data\ArrayDataProvider([
            'allModels' => $consulta2,
        ]),
        'columns' => [
            'nombre',
            'dorsal',
        ],
    ]); ?>
</div>

<div class="consulta3">
    <h1>Resultados de la consulta</h1>
    <?= GridView::widget([
        'dataProvider' => new \yii\data\ArrayDataProvider([
            'allModels' => $consulta3,
        ]),
        'columns' => [
            'nombre',
            'dorsal',
        ],
    ]); ?>
</div>